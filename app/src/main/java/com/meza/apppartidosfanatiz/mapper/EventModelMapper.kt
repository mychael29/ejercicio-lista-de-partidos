package com.meza.apppartidosfanatiz.mapper

import com.meza.apppartidosfanatiz.model.EventModel
import com.meza.domain.entity.Event

interface EventModelMapper {

    suspend fun eventDomainToPresentation(items: List<Event>) : List<EventModel>

}