package com.meza.apppartidosfanatiz.mapper

import com.meza.apppartidosfanatiz.model.StatusModel
import com.meza.apppartidosfanatiz.model.TeamModel
import com.meza.domain.entity.Status
import com.meza.domain.entity.Team


class ItemsFilterMapperImpl: ItemsFilterMapper {

    override fun statusDomainToPresentation(items: List<Status>): List<StatusModel> {
        return items.map {
            StatusModel(itemStatus = it, isActivate = false)
        }
    }

    override fun teamsDomainToPresentation(items: List<Team>): List<TeamModel> {
        return items.map {
            TeamModel(itemTeam = it, isActivate = false)
        }
    }

}