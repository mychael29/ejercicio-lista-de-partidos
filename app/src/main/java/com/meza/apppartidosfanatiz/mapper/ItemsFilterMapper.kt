package com.meza.apppartidosfanatiz.mapper

import com.meza.apppartidosfanatiz.model.StatusModel
import com.meza.apppartidosfanatiz.model.TeamModel
import com.meza.domain.entity.Status
import com.meza.domain.entity.Team

interface ItemsFilterMapper {

    fun statusDomainToPresentation(items: List<Status>): List<StatusModel>

    fun teamsDomainToPresentation(items: List<Team>): List<TeamModel>

}