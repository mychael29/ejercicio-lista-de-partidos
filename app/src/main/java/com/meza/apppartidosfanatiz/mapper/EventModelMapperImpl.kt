package com.meza.apppartidosfanatiz.mapper

import com.meza.apppartidosfanatiz.model.EventModel
import com.meza.domain.entity.Event

class EventModelMapperImpl: EventModelMapper {

    override suspend fun eventDomainToPresentation(items: List<Event>): List<EventModel> {
        return items.map {
            EventModel(itemEvent = it, isCancelled = false)
        }
    }

}