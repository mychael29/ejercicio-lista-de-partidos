package com.meza.apppartidosfanatiz

import com.meza.data.SharedPreferenceDataSource
import com.meza.domain.Constants.PREF_USER_UID

object UserSingleton {
    private var repository: SharedPreferenceDataSource? = null
    fun init(repository: SharedPreferenceDataSource) {
        UserSingleton.repository = repository
    }

    fun getAccessToken(): String = repository?.getString(PREF_USER_UID, "").toString()
    fun setAccessToken(value: String?) = repository?.putString(PREF_USER_UID, value)
}