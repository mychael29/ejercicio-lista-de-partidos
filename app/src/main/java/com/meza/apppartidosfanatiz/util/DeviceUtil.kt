package com.meza.apppartidosfanatiz.util

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Point
import android.os.Build
import android.provider.Settings
import android.telephony.TelephonyManager
import com.meza.apppartidosfanatiz.BuildConfig
import com.meza.domain.entity.App
import com.meza.domain.entity.Authenticate
import com.meza.domain.entity.Device
import com.meza.domain.entity.Profile
import com.meza.domain.entity.User

class DeviceUtil {
    companion object {
        fun getDeviceData(context: Context, point: Point): Authenticate {
            return Authenticate(
                device = Device(
                    deviceId = isIdDevice(context),
                    heigth = point.x.toString(),
                    width = point.y.toString(),
                    model = Build.MODEL,
                    name = getDeviceName().toString(),
                    platform = "android",
                    version = Build.VERSION.SDK_INT.toString()
                ),
                app = App(
                    version = BuildConfig.VERSION_NAME.plus(".0")
                ),
                user = User(
                    _id = "",
                    profile = Profile("es"),
                    rbac = null
                )
            )
        }

        private fun getDeviceName(): String? {
            val manufacturer = Build.MANUFACTURER
            val model = Build.MODEL
            return if (model.toLowerCase().startsWith(manufacturer.toLowerCase())) capitalize(model)
            else capitalize(manufacturer).toString() + " " + model
        }

        private fun capitalize(s: String?): String? {
            if (s == null || s.isEmpty()) return ""
            val first = s[0]
            return if (Character.isUpperCase(first)) s
            else Character.toUpperCase(first).toString() + s.substring(1)
        }

        @SuppressLint("HardwareIds")
        private fun isIdDevice(context: Context): String {
            val deviceId: String
            deviceId = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
            } else {
                val mTelephony = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    if (mTelephony.imei != null) mTelephony.imei
                    else Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
                } else {
                    @Suppress("DEPRECATION")
                    if (mTelephony.deviceId != null) mTelephony.deviceId
                    else Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
                }
            }
            return deviceId
        }

    }
}