package com.meza.apppartidosfanatiz.util

import android.content.Context
import android.content.res.ColorStateList
import android.content.res.Resources
import android.widget.CheckBox
import androidx.core.content.ContextCompat
import androidx.core.widget.CompoundButtonCompat
import com.google.android.material.chip.Chip
import com.meza.apppartidosfanatiz.R

class ViewUtil {
    companion object {
         fun getChip(context: Context?, text: String, isSelected: Boolean): Chip? {
             return if (isSelected) {
                 val chip = Chip(context)
                 chip.text = text
                 chip.chipIcon = context?.let { ContextCompat.getDrawable(it, R.drawable.fanatiz_icon_chip) }
                 chip.isChipIconVisible = true
                 chip.isCloseIconVisible = true
                 chip.isClickable = true
                 chip.isCheckable = false
                 chip
             } else {
                 null
             }
        }

        fun getCheckBox(context: Context?, resources: Resources, text: String, isSelected: Boolean): CheckBox? {
            val scale = resources.displayMetrics.density
            val padding = (10 * scale + 0.5f).toInt()
            val cb = CheckBox(context)
            cb.text = text
            cb.isChecked = isSelected

            context?.let {
                val colors = colorStateListOf(
                    intArrayOf(-android.R.attr.state_enabled) to ContextCompat.getColor(it, R.color.colorGrayText),
                    intArrayOf(android.R.attr.state_enabled) to ContextCompat.getColor(it, R.color.colorWhite)
                )
                CompoundButtonCompat.setButtonTintList(cb, colors)

                cb.setTextColor(ContextCompat.getColor(it, R.color.colorWhite))
            }

            cb.setPadding(0, padding, 0, padding)
            return cb
        }

        private fun colorStateListOf(vararg mapping: Pair<IntArray, Int>): ColorStateList {
            val (states, colors) = mapping.unzip()
            return ColorStateList(states.toTypedArray(), colors.toIntArray())
        }
    }
}