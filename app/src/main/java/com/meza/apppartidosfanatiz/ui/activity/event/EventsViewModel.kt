package com.meza.apppartidosfanatiz.ui.activity.event

import android.os.Bundle
import androidx.lifecycle.*
import com.meza.apppartidosfanatiz.UserSingleton
import com.meza.apppartidosfanatiz.mapper.EventModelMapper
import com.meza.apppartidosfanatiz.model.EventModel
import com.meza.apppartidosfanatiz.ui.*
import com.meza.apppartidosfanatiz.ui.adapter.EventAdapter
import com.meza.domain.Constants.STATUS_DEFAULT
import com.meza.domain.Constants.TEAM_DEFAULT
import com.meza.domain.Failure
import com.meza.domain.entity.Event
import com.meza.domain.entity.EventList
import com.meza.domain.usecase.GetEventUseCase
import javax.inject.Inject

class EventsViewModel @Inject constructor(
    private val getEventUseCase: GetEventUseCase,
    private val mapper: EventModelMapper,
) : BaseViewModel<EventsViewModel>() {

    private val initiateFilter = listOf(STATUS_DEFAULT.joinToString(), TEAM_DEFAULT.joinToString())
    val touchListenerItem = MutableLiveData<Bundle>()

    private val _eventList = MutableLiveData<List<Event>>()
    val eventList : LiveData<List<EventModel>> = _eventList.switchMap {
        liveData {
            emit(mapper.eventDomainToPresentation(it))
        }
    }

    val adapter = EventAdapter(arrayListOf()) { _, _->
        touchListenerItem.value = Bundle()
    }

    fun initiate() {
        handleLoading()
        executeGetEventListUseCase(initiateFilter)
    }

    fun bindItemsAfterMapping(eventListMapped: List<EventModel>) {
        setRefreshingView(false)
        showLoadingView(false)
        showErrorCauseView(false)
        showRvEventListView(true)
        adapter.addItems(eventListMapped)
    }

    private fun executeGetEventListUseCase(params: List<String>) {
        getEventUseCase.invoke(viewModelScope, GetEventUseCase.Params(UserSingleton.getAccessToken(), params)) {
            it.result(::handleUseCaseSuccess, ::handleUseCaseFailure)
        }
    }

    fun refreshData() {
        setRefreshingView(true)
        executeGetEventListUseCase(initiateFilter)
    }

    fun reloadData(params: List<String>) {
        handleLoading()
        executeGetEventListUseCase(params)
    }

    private fun handleLoading() {
        showRvEventListView(false)
        showLoadingView(true)
    }

    private fun handleUseCaseSuccess(eventList: EventList) {
        _eventList.value = eventList.items
    }

    fun restoredEventList(items: List<Event>) {
        _eventList.value = items
    }

    fun saveStateEventList(): List<Event>? {
        return _eventList.value
    }

    private fun handleUseCaseFailure(failure: Failure) {
        handleUseCaseFailureFromBase(failure)
        showErrorCauseView(true)
        showLoadingView(false)
        setRefreshingView(false)
        showRvEventListView(false)
    }

}