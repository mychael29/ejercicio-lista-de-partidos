package com.meza.apppartidosfanatiz.ui.fragment.dialog

import androidx.lifecycle.*
import com.meza.apppartidosfanatiz.UserSingleton
import com.meza.apppartidosfanatiz.mapper.ItemsFilterMapper
import com.meza.apppartidosfanatiz.model.StatusModel
import com.meza.apppartidosfanatiz.model.TeamModel
import com.meza.apppartidosfanatiz.ui.BaseViewModel
import com.meza.domain.Failure
import com.meza.domain.entity.StatusList
import com.meza.domain.entity.TeamList
import com.meza.domain.usecase.GetStatusUseCase
import com.meza.domain.usecase.GetTeamUseCase


class FilterEventViewModel(
    private val getTeamUseCase: GetTeamUseCase,
    private val getStatusUseCase: GetStatusUseCase,
    private val mapper: ItemsFilterMapper
) : BaseViewModel<FilterEventViewModel>() {

    val apply = MutableLiveData<List<String>>()

    private val _statusList = MutableLiveData<List<StatusModel>>()
    val statusList: LiveData<List<StatusModel>> get() = _statusList

    private val _teamList = MutableLiveData<List<TeamModel>>()
    val teamList: LiveData<List<TeamModel>> get() = _teamList

    private val _teamListFiltered = MutableLiveData<List<TeamModel>>()
    val teamListFiltered: LiveData<List<TeamModel>> get() = _teamListFiltered

    private val _statusListFiltered = MutableLiveData<List<StatusModel>>()
    val statusListFiltered: LiveData<List<StatusModel>> get() = _statusListFiltered

    fun setTeamList(teamModelList: List<TeamModel>) {
        _teamListFiltered.value = teamModelList.filter { it.isActivate }
    }

    fun setStatusList(statusModelList: List<StatusModel>) {
        _statusListFiltered.value = statusModelList
    }

    private fun executeGetTeamListUseCase() {
        getTeamUseCase.invoke(viewModelScope, GetTeamUseCase.Params(UserSingleton.getAccessToken())) {
            it.result(::handleTeamUseCaseSuccess, ::handleUseCaseFailure)
        }
    }

    private fun executeGetStatusListUseCase() {
        getStatusUseCase.invoke(viewModelScope, GetStatusUseCase.Params(UserSingleton.getAccessToken())) {
            it.result(::handleStatusUseCaseSuccess, ::handleUseCaseFailure)
        }
    }

    private fun handleTeamUseCaseSuccess(teamList : TeamList) {
        _teamList.value = mapper.teamsDomainToPresentation(teamList.items)
        handleFilterSuccess()
    }

    private fun handleStatusUseCaseSuccess(statusList : StatusList) {
        _statusList.value = mapper.statusDomainToPresentation(statusList.items)
    }

    fun getFilterData() {
        viewLoadingFilter()
        executeGetStatusListUseCase()
        executeGetTeamListUseCase()
    }

    fun applyFilter() {
        apply.value = listOf(
            (statusListFiltered.value ?: listOf()).joinToString(),
            (teamListFiltered.value ?: listOf()).joinToString()
        )
    }

    private fun viewLoadingFilter() {
        showLoadingView(true)
        showErrorCauseView(false)
        showCloseButtonView(false)
        showFilterView(false)
        showApplyButtonView(false)
    }

    private fun handleFilterSuccess() {
        showLoadingView(false)
        showErrorCauseView(false)
        showCloseButtonView(true)
        showFilterView(true)
        showApplyButtonView(true)
    }

    private fun handleUseCaseFailure(failure: Failure) {
        handleUseCaseFailureFromBase(failure)
        showLoadingView(false)
        showErrorCauseView(true)
        showCloseButtonView(true)
        showFilterView(false)
        showApplyButtonView(false)
        setRefreshingView(false)
    }

    fun handleFailureRestored() {
        handleUseCaseFailureFromBase(Failure.None)
        showErrorCauseView(true)
        showCloseButtonView(true)
    }

    fun clearDataList() {
        _teamListFiltered.value = listOf()
        _statusListFiltered.value = listOf()
    }

    fun restoredStateTeamList(teamModelList: List<TeamModel>) {
        _teamList.value = teamModelList
        handleFilterSuccess()
    }
    fun restoredStateStatusList(statusModelList: List<StatusModel>) {
        _statusList.value = statusModelList
    }

}