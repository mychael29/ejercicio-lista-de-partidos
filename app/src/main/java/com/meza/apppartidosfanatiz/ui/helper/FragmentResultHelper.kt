package com.meza.apppartidosfanatiz.ui.helper

interface FragmentResultHelper {
    fun setFragmentResult(list: List<String>)
}