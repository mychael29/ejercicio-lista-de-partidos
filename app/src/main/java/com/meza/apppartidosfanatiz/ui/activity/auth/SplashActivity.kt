package com.meza.apppartidosfanatiz.ui.activity.auth

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Point
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.meza.apppartidosfanatiz.R
import com.meza.apppartidosfanatiz.core.di.viewmodel.ViewModelFactory
import com.meza.apppartidosfanatiz.ui.*
import com.meza.apppartidosfanatiz.ui.BaseActivity
import com.meza.apppartidosfanatiz.ui.activity.event.EventsActivity
import com.meza.apppartidosfanatiz.ui.helper.DialogPermissionHelper
import com.meza.apppartidosfanatiz.util.DeviceUtil
import com.meza.apppartidosfanatiz.util.DialogUtil
import com.meza.domain.Constants.DENIED
import com.meza.domain.Constants.GRANTED
import com.meza.domain.Constants.INITIAL_REQUEST
import com.meza.domain.entity.Authenticate
import dagger.android.AndroidInjection.inject
import javax.inject.Inject


class SplashActivity : AppCompatActivity(), DialogPermissionHelper, DialogUtil.OnEventDialog {
    @Inject
    lateinit var providerFactory: ViewModelFactory
    private lateinit var viewModel: SplashViewModel
    private lateinit var authenticate: Authenticate

    companion object {
        val INITIAL_PERMS = arrayOf(
            Manifest.permission.READ_PHONE_STATE
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        inject(this)

        viewModel = ViewModelProvider(this, providerFactory).get(SplashViewModel::class.java)

        when (hasPermission()) {
            true -> initiateAuth()
            false -> callRequestPermission()
        }

        observeViewState()
    }

    private fun observeViewState() {
        viewModel.dataState.observe(this, Observer {
            when (it) {
                is NotInitiate -> DialogUtil.showDialogListener(this, it.message, this)
                is Initiate -> initiateApp()
            }
        })
    }

    private fun initiateApp() {
        startActivity(Intent(this, EventsActivity::class.java))
        this.finish()
    }

    private fun initiateAuth() {
        instantiate()
        viewModel.startAuthenticate(authenticate)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        when (requestCode) {
            INITIAL_REQUEST -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initiateAuth()
                } else {
                    DialogUtil.showMessageDialogPermission(this, getString(R.string.message_permission), this)
                }
                return
            }
        }
    }

    private fun hasPermission(): Boolean {
        return PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(this, INITIAL_PERMS[0])
    }

    private fun statusPermission(): Boolean {
        return getPermissionStatus(INITIAL_PERMS[0])
    }

    private fun getPermissionStatus(permission: String): Boolean {
        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                return DENIED
            }
        }
        return GRANTED
    }

    private fun callRequestPermission() {
        requestPermissions(INITIAL_PERMS, INITIAL_REQUEST)
    }

    override fun finish(status: Boolean?) {
        status?.let {
            when(it) {
                true -> finish()
                false -> { }
            }
        }
    }

    override fun requestPermission() {
        when(statusPermission()) {
            true -> initiateAuth()
            false ->  callRequestPermission()
        }
    }

    private fun instantiate() {
        val size = Point()
        windowManager.defaultDisplay.getSize(size)
        authenticate = DeviceUtil.getDeviceData(this, size)
    }

    override fun onClickAccept() {
        finish()
    }

}