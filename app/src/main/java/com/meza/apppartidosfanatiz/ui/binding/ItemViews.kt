package com.meza.apppartidosfanatiz.ui.binding

import android.view.View
import android.widget.CompoundButton
import android.widget.LinearLayout
import androidx.databinding.BindingAdapter
import com.google.android.material.chip.ChipGroup
import com.meza.apppartidosfanatiz.model.StatusModel
import com.meza.apppartidosfanatiz.model.TeamModel
import com.meza.apppartidosfanatiz.util.ViewUtil

@BindingAdapter("visible")
fun View.bindVisible(boolean: Boolean?){
    alpha = 0.2f
    animate().alpha(1f)
    visibility = if (boolean == true) View.VISIBLE else View.GONE
}

@BindingAdapter("addCheckBoxListStatus")
fun LinearLayout.addViewCheckBoxListFilter(statusModelList: List<StatusModel>?) {
    statusModelList?.forEach {
        val cb = ViewUtil.getCheckBox(context, this.resources, it.itemStatus.name, it.isActivate)
        cb?.let { _ ->
            addView(cb)
            cb.setOnCheckedChangeListener { _: CompoundButton?, b: Boolean ->
                it.isActivate = b
            }
        }
    }
}

@BindingAdapter("addChipListTeam")
fun ChipGroup.addViewChipFilter(teamModelList: List<TeamModel>?) {
    teamModelList?.forEach {
        ViewUtil.getChip(context, it.itemTeam.name, it.isActivate)?.let {chip ->
            addView(chip as View)
            chip.setOnCloseIconClickListener {_->
                removeView(chip as View)
                it.isActivate = false
            }
        }
    }
}

