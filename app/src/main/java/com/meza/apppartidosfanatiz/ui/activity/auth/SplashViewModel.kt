package com.meza.apppartidosfanatiz.ui.activity.auth

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.meza.apppartidosfanatiz.UserSingleton
import com.meza.apppartidosfanatiz.ui.*
import com.meza.domain.Failure
import com.meza.domain.entity.Authenticate
import com.meza.domain.entity.Authenticator
import com.meza.domain.usecase.GetAuthUseCase
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    private val getAuthUseCase: GetAuthUseCase
) : BaseViewModel<SplashViewModel>() {

    private val _dataState = MutableLiveData<StateInitiate>()
    val dataState: LiveData<StateInitiate> get() = _dataState

    fun startAuthenticate(authenticate: Authenticate) {
        getAuthUseCase.invoke(viewModelScope, GetAuthUseCase.Params(authenticate)) {
            it.result(::handleInitiate, ::handleNotInitiate)
        }
    }

    private fun handleInitiate(authenticator: Authenticator) {
        _dataState.value = Initiate
        UserSingleton.setAccessToken(authenticator.accessToken)
    }

    private fun handleNotInitiate(failure: Failure) {
        _dataState.value = NotInitiate(handleUseCaseFailureFromInitiate(failure))
    }

}