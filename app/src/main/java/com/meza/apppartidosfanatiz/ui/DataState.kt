package com.meza.apppartidosfanatiz.ui


sealed class StateInitiate
object Initiate : StateInitiate()
data class NotInitiate(val message: String)  : StateInitiate()




