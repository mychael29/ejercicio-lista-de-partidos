package com.meza.apppartidosfanatiz.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.meza.apppartidosfanatiz.R
import com.meza.apppartidosfanatiz.databinding.ItemTeamBinding
import com.meza.apppartidosfanatiz.model.TeamModel

class ItemsFilterAdapter(
    context: Context?,
    items: List<TeamModel>,
    private val onClickListener: OnClickListener
) : ArrayAdapter<TeamModel>(context!!, 0, items) {

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val binding : ItemTeamBinding? = DataBindingUtil.bind(LayoutInflater.from(parent.context).inflate(R.layout.item_team, parent, false))
        val currentItem = getItem(position)
        val removeCardText = binding!!.root.findViewById<LinearLayout>(R.id.llContainer)

        binding.titleTeam.text = currentItem?.itemTeam?.name

        removeCardText.setOnClickListener{
            currentItem?.let {
                it.isActivate = true
                onClickListener.onClick(it)
            }
        }
        return binding.root
    }

    class OnClickListener(val clickListener: (team: TeamModel) -> Unit) {
        fun onClick(team: TeamModel) = clickListener(team)
    }

}