package com.meza.apppartidosfanatiz.ui.fragment.dialog

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.meza.apppartidosfanatiz.BR
import com.meza.apppartidosfanatiz.R
import com.meza.apppartidosfanatiz.core.di.viewmodel.ViewModelFactory
import com.meza.apppartidosfanatiz.databinding.FragmentFilterTeamsBinding
import com.meza.apppartidosfanatiz.model.StatusModel
import com.meza.apppartidosfanatiz.model.TeamModel
import com.meza.apppartidosfanatiz.ui.adapter.ItemsFilterAdapter
import com.meza.apppartidosfanatiz.ui.helper.FragmentResultHelper
import com.meza.apppartidosfanatiz.util.DialogUtil
import com.meza.domain.Constants.DELAY_AFTER_USER_INPUT_FOR_AUTOCOMPLETE
import com.meza.domain.Constants.IS_ERROR_SAVE
import com.meza.domain.Constants.IS_LOADING_SAVE
import com.meza.domain.Constants.STATUS_SAVE
import com.meza.domain.Constants.TEAMS_SAVE
import dagger.android.support.AndroidSupportInjection.inject
import java.io.Serializable
import javax.inject.Inject

class FilterEventFragment: DialogFragment() {
    @Inject
    lateinit var providerFactory: ViewModelFactory
    private lateinit var viewModel: FilterEventViewModel
    private lateinit var viewDataBinding: FragmentFilterTeamsBinding
    private lateinit var rootView: View
    private lateinit var fragmentResultHelper: FragmentResultHelper

    private var handler: Handler = Handler()

    companion object {
        fun newInstance() = FilterEventFragment()
    }

    fun setResult(fragmentResultHelper: FragmentResultHelper) {
        this.fragmentResultHelper = fragmentResultHelper
    }

    private val itemsFilterAdapter: ItemsFilterAdapter by lazy {
        ItemsFilterAdapter(context, mutableListOf(), ItemsFilterAdapter.OnClickListener {
            viewModel.setTeamList(listOf(it))
            viewDataBinding.tvaTeamSearch.text.clear()
            itemsFilterAdapter.clear()
            itemsFilterAdapter.addAll(mutableListOf())
            itemsFilterAdapter.notifyDataSetChanged()
            itemsFilterAdapter.filter.filter("")
        })
    }

    override fun onStart() {
        super.onStart()
        DialogUtil.configDialogFragment(this, resources)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_filter_teams, container, false)
        rootView = viewDataBinding.root
        return rootView
    }

    override fun onAttach(context: Context) {
        inject(this)
        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this, providerFactory).get(FilterEventViewModel::class.java)

        bindDataBinding()

        isCancelable = false

        subscribeObservers()

        savedInstanceState ?: viewModel.getFilterData()

        viewDataBinding.btnCloseDialog.setOnClickListener { dismiss() }
    }

    private fun bindDataBinding() {
        viewDataBinding.setVariable(BR.filterEventViewModel, viewModel)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        viewDataBinding.executePendingBindings()
    }

    private fun subscribeObservers() {
        viewModel.teamList.observe(viewLifecycleOwner, Observer {
            addAutoCompleteTextView(it)
            viewModel.setTeamList(it)
        })

        viewModel.statusList.observe(viewLifecycleOwner, Observer {
            viewModel.setStatusList(it)
        })

        viewModel.apply.observe(viewLifecycleOwner, Observer {
            fragmentResultHelper.setFragmentResult(it)
            dismiss()
        })
    }

    private fun addAutoCompleteTextView(items: List<TeamModel>) {
        viewDataBinding.tvaTeamSearch.setAdapter(itemsFilterAdapter)

        viewDataBinding.tvaTeamSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) { }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                handler.removeCallbacksAndMessages(null)
                handler.postDelayed({
                    if (isAdded) {
                        context?.let {
                            activity?.runOnUiThread {
                                itemsFilterAdapter.clear()
                                itemsFilterAdapter.addAll(items)
                                itemsFilterAdapter.notifyDataSetChanged()
                                itemsFilterAdapter.filter.filter(p0)
                            }
                        }
                    }
                }, DELAY_AFTER_USER_INPUT_FOR_AUTOCOMPLETE)
            }
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        viewModel.clearDataList()
        viewModel.statusList.value?.let { outState.putSerializable(STATUS_SAVE, it as Serializable) }
        viewModel.teamList.value?.let { outState.putSerializable(TEAMS_SAVE, it as Serializable) }
        viewModel.isLoading.value?.let { outState.putBoolean(IS_LOADING_SAVE, it) }
        viewModel.showErrorCause.value?.let { outState.putBoolean(IS_ERROR_SAVE, it) }
        super.onSaveInstanceState(outState)
    }

    @Suppress("UNCHECKED_CAST")
    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        recoverInterfaceHelper()
        savedInstanceState?.getSerializable(STATUS_SAVE)?.let { viewModel.restoredStateStatusList(it as List<StatusModel>) }
        savedInstanceState?.getSerializable(TEAMS_SAVE)?.let { viewModel.restoredStateTeamList(it as List<TeamModel>) }
        savedInstanceState?.getBoolean(IS_LOADING_SAVE).let { if (it == true) viewModel.getFilterData() }
        savedInstanceState?.getBoolean(IS_ERROR_SAVE).let { if (it == true) viewModel.handleFailureRestored() }
        super.onViewStateRestored(savedInstanceState)
    }

    private fun recoverInterfaceHelper() {
        if (context is FragmentResultHelper) {
            fragmentResultHelper = (context as FragmentResultHelper)
        }
    }

}