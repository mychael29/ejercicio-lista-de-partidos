package com.meza.apppartidosfanatiz.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.meza.apppartidosfanatiz.BR
import com.meza.apppartidosfanatiz.R
import com.meza.apppartidosfanatiz.databinding.ItemEventBinding
import com.meza.apppartidosfanatiz.model.EventModel

class EventAdapter (private val items: MutableList<EventModel>,
                    val callback:(model: EventModel, position: Int) -> Unit) : RecyclerView.Adapter<EventAdapter.EventHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventHolder {
        val binding : ItemEventBinding? = DataBindingUtil.bind(LayoutInflater.from(parent.context).inflate(R.layout.item_event, parent, false))
        return EventHolder(binding!!)
    }

    override fun onBindViewHolder(holder: EventHolder, position: Int) {
        val todoModel = items[position]
        holder.setItem(todoModel)
        holder.itemEventBinding.root.setOnClickListener {
            callback(todoModel, position)
        }
    }

    fun addItems(newItems: List<EventModel>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    inner class EventHolder(val itemEventBinding: ItemEventBinding) : RecyclerView.ViewHolder(itemEventBinding.root) {
        fun setItem(model: EventModel) {
            itemEventBinding.setVariable(BR.modelEvent, model)
            itemEventBinding.executePendingBindings()
        }
    }

    override fun getItemCount() = items.size

}