package com.meza.apppartidosfanatiz.ui.binding

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.meza.apppartidosfanatiz.ui.adapter.EventAdapter

@BindingAdapter("eventAdapter")
fun setEventAdapter(recyclerView: RecyclerView, adapter: EventAdapter){
    recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
    recyclerView.adapter = adapter
}