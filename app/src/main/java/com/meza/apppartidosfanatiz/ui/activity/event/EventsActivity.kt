package com.meza.apppartidosfanatiz.ui.activity.event

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import com.meza.apppartidosfanatiz.BR
import com.meza.apppartidosfanatiz.R
import com.meza.apppartidosfanatiz.databinding.ActivityEventsBinding
import com.meza.apppartidosfanatiz.ui.fragment.dialog.FilterEventFragment
import com.meza.apppartidosfanatiz.ui.BaseActivity
import com.meza.apppartidosfanatiz.ui.activity.event.detail.EventDetailActivity
import com.meza.apppartidosfanatiz.ui.helper.FragmentResultHelper
import com.meza.domain.Constants.EVENTS_SAVE
import com.meza.domain.Constants.FILTER_DIALOG
import com.meza.domain.entity.Event
import dagger.android.AndroidInjection.inject
import java.io.Serializable

class EventsActivity : BaseActivity<EventsViewModel, ActivityEventsBinding>(), FragmentResultHelper {

    override fun getViewBinding() = ActivityEventsBinding.inflate(layoutInflater)

    override fun getBindingVariable(): Int = BR.eventsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject(this)
        savedInstanceState ?: viewModel.initiate()
        observeViewState()
    }

    private fun observeViewState() {
        viewModel.eventList.observe(this, Observer {
            viewModel.bindItemsAfterMapping(it)
        })

        viewModel.touchListenerItem.observe(this, Observer {
            startActivity(Intent(this, EventDetailActivity::class.java))
        })
    }

    private fun showDialogFragment() {
        val fragmentDialog = FilterEventFragment.newInstance()
        fragmentDialog.setResult(this)
        fragmentDialog.show(supportFragmentManager, FILTER_DIALOG)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_filter_events, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.action_fav -> {
            showDialogFragment()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        viewModel.saveStateEventList()?.let { outState.putSerializable(EVENTS_SAVE, it as Serializable) }
        super.onSaveInstanceState(outState)
    }

    @Suppress("UNCHECKED_CAST")
    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        savedInstanceState.getSerializable(EVENTS_SAVE)?.let { viewModel.restoredEventList(it as List<Event>) }
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun setFragmentResult(list: List<String>) {
        viewModel.reloadData(list)
    }

}