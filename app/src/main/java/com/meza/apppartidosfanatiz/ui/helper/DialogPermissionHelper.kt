package com.meza.apppartidosfanatiz.ui.helper

interface DialogPermissionHelper {
    fun finish(status: Boolean?)
    fun requestPermission()
}