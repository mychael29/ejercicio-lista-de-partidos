package com.meza.apppartidosfanatiz.ui.activity.event.detail

import android.os.Bundle
import com.meza.apppartidosfanatiz.BR
import com.meza.apppartidosfanatiz.databinding.ActivityEventDetailBinding
import com.meza.apppartidosfanatiz.ui.BaseActivity

class EventDetailActivity: BaseActivity<EventDetailViewModel, ActivityEventDetailBinding>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun getViewBinding(): ActivityEventDetailBinding = ActivityEventDetailBinding.inflate(layoutInflater)

    override fun getBindingVariable(): Int = BR.eventDetailViewModel
}