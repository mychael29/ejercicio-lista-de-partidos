package com.meza.apppartidosfanatiz.model

import com.meza.domain.entity.Event

data class EventModel (
    val itemEvent: Event,
    var isCancelled: Boolean
)