package com.meza.apppartidosfanatiz.model

import com.meza.domain.entity.Team
import java.io.Serializable

data class TeamModel (
    val itemTeam: Team,
    var isActivate: Boolean
): Serializable {
    override fun toString(): String {
        return if (isActivate) {
            itemTeam.id
        } else {
            itemTeam.name
        }
    }
}