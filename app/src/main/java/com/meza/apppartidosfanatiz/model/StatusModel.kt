package com.meza.apppartidosfanatiz.model

import com.meza.domain.entity.Status
import java.io.Serializable


data class StatusModel (
    val itemStatus: Status,
    var isActivate: Boolean
): Serializable {
    override fun toString(): String {
        return itemStatus.name
    }
}