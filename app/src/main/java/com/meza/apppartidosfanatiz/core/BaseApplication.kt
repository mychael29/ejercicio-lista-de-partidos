package com.meza.apppartidosfanatiz.core

import com.meza.data.SharedPreferenceDataSource
import com.meza.apppartidosfanatiz.core.di.DaggerAppComponent
import com.meza.apppartidosfanatiz.UserSingleton
import com.meza.apppartidosfanatiz.util.ConnectionUtil
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class BaseApplication : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
        UserSingleton.init(
            SharedPreferenceDataSource(
                applicationContext
            )
        )
        ConnectionUtil.init(applicationContext)
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val androidInjector: AndroidInjector<BaseApplication> =
            DaggerAppComponent.builder().application(this).build()
        androidInjector.inject(this)
        return androidInjector
    }
}