package com.meza.apppartidosfanatiz.core.di.modules

import com.meza.data.network.service.FanatizServiceImpl
import com.meza.data.network.apis.FanatizApi
import com.meza.data.network.service.FanatizService
import com.meza.domain.Constants.AUTH_TOKEN
import com.meza.domain.Constants.URL_BASE
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun serviceApi(fanatizApi: FanatizApi): FanatizService =
        FanatizServiceImpl(fanatizApi)

    @Provides
    @Singleton
    fun authApi(retrofit: Retrofit): FanatizApi = retrofit.create(
        FanatizApi::class.java)

    @Provides
    @Singleton
    fun retrofitInterface(): Retrofit = Retrofit.Builder()
        .baseUrl(URL_BASE)
        .addConverterFactory(GsonConverterFactory.create())
        .client(OkHttpClient.Builder().addInterceptor {
            val request = it.request().newBuilder()
                .addHeader("Content-Type", "application/json")
                .addHeader("Accept", "application/json")
                .addHeader("Authorization", AUTH_TOKEN)
                .build()
            return@addInterceptor it.proceed(request)
        }.build())
        .build()
}