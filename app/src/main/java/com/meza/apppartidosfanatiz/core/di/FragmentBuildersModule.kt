package com.meza.apppartidosfanatiz.core.di

import com.meza.apppartidosfanatiz.core.di.modules.AuthScope
import com.meza.apppartidosfanatiz.core.di.viewmodel.ViewModelModule
import com.meza.apppartidosfanatiz.ui.fragment.dialog.FilterEventFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {
    @AuthScope
    @ContributesAndroidInjector(modules = [ViewModelModule::class])
    abstract fun contributeFilterFragment(): FilterEventFragment?

}