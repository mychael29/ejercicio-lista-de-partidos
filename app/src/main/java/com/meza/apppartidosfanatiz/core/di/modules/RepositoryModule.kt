package com.meza.apppartidosfanatiz.core.di.modules

import com.meza.data.mapper.*
import com.meza.domain.repository.AuthRepository
import com.meza.data.repository.AuthRepositoryImpl
import com.meza.data.network.service.FanatizService
import com.meza.data.repository.EventRepositoryImpl
import com.meza.data.repository.FilterRepositoryImpl
import com.meza.domain.repository.EventRepository
import com.meza.domain.repository.FilterRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {
    @Provides
    @Singleton
    fun provideAuthRepository(service: FanatizService, authMapper: AuthMapper): AuthRepository {
        return AuthRepositoryImpl(
            service,
            authMapper
        )
    }

    @Provides
    @Singleton
    fun provideEventRepository(service: FanatizService, eventsMapper: EventsMapper): EventRepository {
        return EventRepositoryImpl(
            service,
            eventsMapper
        )
    }

    @Provides
    @Singleton
    fun provideFilterRepository(service: FanatizService, filterMapper: FilterMapper): FilterRepository {
        return FilterRepositoryImpl(
            service,
            filterMapper
        )
    }

    @Provides
    @Singleton
    fun provideAuthMapper(): AuthMapper {
        return AuthMapperImpl()
    }

    @Provides
    @Singleton
    fun provideEventMapper(): EventsMapper {
        return EventsMapperImpl()
    }

    @Provides
    @Singleton
    fun provideFilterMapper(): FilterMapper {
        return FilterMapperImpl()
    }

}