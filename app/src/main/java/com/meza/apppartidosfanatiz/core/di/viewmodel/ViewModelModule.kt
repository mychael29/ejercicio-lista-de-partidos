package com.meza.apppartidosfanatiz.core.di.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.meza.apppartidosfanatiz.mapper.EventModelMapper
import com.meza.apppartidosfanatiz.mapper.EventModelMapperImpl
import com.meza.apppartidosfanatiz.mapper.ItemsFilterMapper
import com.meza.apppartidosfanatiz.mapper.ItemsFilterMapperImpl
import com.meza.domain.repository.AuthRepository
import com.meza.domain.usecase.GetAuthUseCase
import com.meza.apppartidosfanatiz.ui.activity.auth.SplashViewModel
import com.meza.apppartidosfanatiz.ui.activity.event.EventsViewModel
import com.meza.apppartidosfanatiz.ui.activity.event.detail.EventDetailViewModel
import com.meza.apppartidosfanatiz.ui.fragment.dialog.FilterEventViewModel
import com.meza.domain.repository.EventRepository
import com.meza.domain.repository.FilterRepository
import com.meza.domain.usecase.GetEventUseCase
import com.meza.domain.usecase.GetStatusUseCase
import com.meza.domain.usecase.GetTeamUseCase
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Provider


@Module
class ViewModelModule {
    @Provides
    fun provideViewModelFactory(providers: MutableMap<Class<out ViewModel>,
            @JvmSuppressWildcards Provider<ViewModel>>): ViewModelProvider.Factory =
        ViewModelFactory(providers)

    @IntoMap
    @Provides
    @ViewModelKey(SplashViewModel::class)
    fun authViewModel(repository: AuthRepository): ViewModel = SplashViewModel(
        GetAuthUseCase(
            repository
        )
    )

    @IntoMap
    @Provides
    @ViewModelKey(EventsViewModel::class)
    fun eventsViewModel(repository: EventRepository, eventModelMapper: EventModelMapper): ViewModel
            = EventsViewModel(GetEventUseCase(repository), eventModelMapper)

    @IntoMap
    @Provides
    @ViewModelKey(FilterEventViewModel::class)
    fun filterViewModel(repository: FilterRepository, itemsFilterMapper: ItemsFilterMapper): ViewModel
            = FilterEventViewModel(GetTeamUseCase(repository), GetStatusUseCase(repository), itemsFilterMapper)

    @IntoMap
    @Provides
    @ViewModelKey(EventDetailViewModel::class)
    fun eventDetailViewModel(): ViewModel = EventDetailViewModel()

    @Provides
    fun provideEventModelMapper(): EventModelMapper {
        return EventModelMapperImpl()
    }

    @Provides
    fun provideFilterModelsMapper(): ItemsFilterMapper {
        return ItemsFilterMapperImpl()
    }

}