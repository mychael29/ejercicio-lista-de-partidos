package com.meza.apppartidosfanatiz.core.di

import com.meza.apppartidosfanatiz.core.di.modules.AuthScope
import com.meza.apppartidosfanatiz.core.di.viewmodel.ViewModelModule
import com.meza.apppartidosfanatiz.ui.activity.auth.SplashActivity
import com.meza.apppartidosfanatiz.ui.activity.event.EventsActivity
import com.meza.apppartidosfanatiz.ui.activity.event.detail.EventDetailActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuildersModule {
    @AuthScope
    @ContributesAndroidInjector(modules = [ViewModelModule::class])
    abstract fun contributeAuthActivity(): SplashActivity?

    @AuthScope
    @ContributesAndroidInjector(modules = [ViewModelModule::class])
    abstract fun contributeEventsActivity(): EventsActivity?

    @AuthScope
    @ContributesAndroidInjector(modules = [ViewModelModule::class])
    abstract fun contributeEventDetailActivity(): EventDetailActivity?

}