package com.meza.domain.entity

import java.io.Serializable


data class TeamList(
    var items: List<Team>
)

data class Team(
    val _id: String,
    val id: String,
    val name: String,
    val shortName: String
): Serializable

data class StatusList(
    var items: List<Status>
)

data class Status(
    val _id: String,
    val name: String
): Serializable