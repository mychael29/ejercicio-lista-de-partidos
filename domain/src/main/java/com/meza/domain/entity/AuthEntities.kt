package com.meza.domain.entity


data class Authenticator(
    val accessToken: String,
    val expiresIn: String,
    val tokenType: String,
    val user: User
)

data class User(
    val _id: String? = null,
    val profile: Profile,
    val rbac: Rbac? = null
)

data class Profile(
    val language: String
)

data class Rbac(
    val role: String,
    val template: String
)


data class Authenticate(
    val app: App,
    val device: Device,
    val user: User
)

data class App(
    val version: String
)

data class Device(
    val deviceId: String,
    val heigth: String,
    val model: String,
    val name: String,
    val platform: String,
    val version: String,
    val width: String
)





