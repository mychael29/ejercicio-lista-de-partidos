package com.meza.domain.entity

import java.io.Serializable

data class EventList(
    val items: List<Event>,
    val pagination: Pagination?,
    val sections: List<Section>?
)

data class Event(
    val _id: String,
    val awayScore: Int,
    val awayTeam: AwayTeam,
    val cellType: Int,
    val endDate: String,
    val eventStatus: EventStatus,
    val homeScore: Int,
    val homeTeam: HomeTeam,
    val location: Location,
    val matchDay: MatchDay,
    val matchTime: Int,
    val startDate: String,
    val statusCategory: String,
    val type: String
): Serializable

data class Pagination(
    val first: Int,
    val items: Int,
    val last: Int,
    val next: Int,
    val offset: Int,
    val page: Int,
    val pages: Int,
    val totalItems: Int
): Serializable

data class Section(
    val _id: String,
    val index: Int,
    val title: Title
): Serializable

data class EventStatus(
    val _id: String,
    val category: String,
    val name: Name
): Serializable

data class HomeTeam(
    val _id: String,
    val name: String,
    val shortName: String
): Serializable

data class AwayTeam(
    val _id: String,
    val name: String,
    val shortName: String
): Serializable

data class Location(
    val original: String
): Serializable

data class MatchDay(
    val end: String,
    val name: Name,
    val phase: Phase,
    val start: String
): Serializable

data class Name(
    val es: String? = null,
    val original: String
): Serializable

data class Phase(
    val original: String
): Serializable

data class Title(
    val original: String
): Serializable


