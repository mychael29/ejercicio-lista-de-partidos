package com.meza.domain.usecase

import com.meza.domain.Failure
import com.meza.domain.entity.EventList
import com.meza.domain.entity.Result
import com.meza.domain.repository.EventRepository

class GetEventUseCase (private val repository: EventRepository):  BaseUseCase<EventList, GetEventUseCase.Params>() {

    override suspend fun run(params: Params): Result<EventList, Failure>
            = repository.getEvents(params.accessToken, params.filter)

    data class Params(val accessToken: String, val filter: List<String>)

}