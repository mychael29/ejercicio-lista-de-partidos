package com.meza.domain.usecase

import com.meza.domain.Failure
import com.meza.domain.entity.Result
import com.meza.domain.entity.TeamList
import com.meza.domain.repository.FilterRepository

class GetTeamUseCase(private val repository: FilterRepository):  BaseUseCase<TeamList, GetTeamUseCase.Params>() {

    override suspend fun run(params: GetTeamUseCase.Params): Result<TeamList, Failure>
            = repository.getTeamList(params.accessToken)

    data class Params(val accessToken: String)

}