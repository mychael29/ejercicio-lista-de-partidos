package com.meza.domain.usecase

import com.meza.domain.Failure
import com.meza.domain.entity.Result
import com.meza.domain.entity.StatusList
import com.meza.domain.repository.FilterRepository


class GetStatusUseCase(private val repository: FilterRepository):  BaseUseCase<StatusList, GetStatusUseCase.Params>() {

    override suspend fun run(params: Params): Result<StatusList, Failure> = repository.getStatusList(params.accessToken)

    data class Params(val accessToken: String)

}