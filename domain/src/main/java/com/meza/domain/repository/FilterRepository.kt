package com.meza.domain.repository

import com.meza.domain.Failure
import com.meza.domain.entity.Result
import com.meza.domain.entity.StatusList
import com.meza.domain.entity.TeamList

interface FilterRepository {

    suspend fun getTeamList(accessToken: String): Result<TeamList, Failure>

    suspend fun getStatusList(accessToken: String): Result<StatusList, Failure>

}