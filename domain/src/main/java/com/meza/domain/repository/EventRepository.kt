package com.meza.domain.repository

import com.meza.domain.Failure
import com.meza.domain.entity.EventList
import com.meza.domain.entity.Result


interface EventRepository {

    suspend fun getEvents(accessToken: String, filter : List<String>): Result<EventList, Failure>

}