package com.meza.domain


object Constants {
    const val URL_BASE = "https://fxservicesstaging.nunchee.com/api/1.0/"
    const val PREF_USER_UID = "AUTH_FANATIZ"
    const val AUTH_TOKEN = "Basic cHJ1ZWJhc2RldjpwcnVlYmFzZGV2U2VjcmV0" //
    const val GRANTED = true
    const val DENIED = false
    const val INITIAL_REQUEST = 1
    const val FILTER_DIALOG = "TEAMS_FRAGMENT"
    const val DELAY_AFTER_USER_INPUT_FOR_AUTOCOMPLETE = 1000L

    const val STATUS = 0
    const val TEAMS = 1
    const val EVENTS = 2

    val STATUS_DEFAULT = arrayOf("live", "finished", "future")
    val TEAM_DEFAULT = arrayOf("")

    const val EVENTS_SAVE = "eventList_Save"
    const val STATUS_SAVE = "statusList_Save"
    const val TEAMS_SAVE = "teamList_Save"
    const val IS_LOADING_SAVE = "is_Loading_Save"
    const val IS_ERROR_SAVE = "is_Error_Save"

}