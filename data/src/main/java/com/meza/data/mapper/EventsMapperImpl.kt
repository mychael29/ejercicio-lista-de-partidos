package com.meza.data.mapper

import com.meza.data.network.response.*
import com.meza.domain.entity.AwayTeam
import com.meza.domain.entity.EventStatus
import com.meza.domain.entity.EventList
import com.meza.domain.entity.HomeTeam
import com.meza.domain.entity.Event
import com.meza.domain.entity.Location
import com.meza.domain.entity.MatchDay
import com.meza.domain.entity.Name
import com.meza.domain.entity.Pagination
import com.meza.domain.entity.Phase
import com.meza.domain.entity.Section
import com.meza.domain.entity.Team
import com.meza.domain.entity.Title
import javax.inject.Inject

class EventsMapperImpl: EventsMapper {

    private fun mapItemList(responseList: List<EventData>): List<Event> {
        return responseList.map { (mapItem(it)) }
    }

    private fun mapSelectionList(responseList: List<SectionData>): List<Section> {
        return responseList.map { (mapItem(it)) }
    }

    private fun mapItem(response: SectionData): Section {
        return Section(
            _id = response._id,
            index = response.index,
            title = Title(original = response.title.original)
        )
    }

    private fun mapItem(response: EventData): Event {
        return Event(
            _id = response._id,
            awayTeam = mapAwayTeam(response.awayTeam),
            homeTeam = mapHomeTeam(response.homeTeam),
            eventStatus = mapEventStatus(response.eventStatus),
            location = mapLocation(response.location),
            matchDay = mapMatchDay(response.matchDay),
            awayScore = response.awayScore,
            cellType = response.cellType,
            endDate = response.endDate,
            startDate = response.startDate,
            matchTime = response.matchTime,
            homeScore = response.homeScore,
            statusCategory = response.statusCategory,
            type = response.type
        )
    }

    private fun mapAwayTeam(response: AwayTeamData): AwayTeam {
        return AwayTeam(
            response._id,
            response.name,
            response.shortName
        )
    }

    private fun mapEventStatus(response: EventStatusData): EventStatus {
        return EventStatus(
            response._id,
            response.category,
            mapName(response.name)
        )
    }

    private fun mapName(response: NameData): Name {
        return Name(response.es, response.original)
    }

    private fun mapHomeTeam(response: HomeTeamData): HomeTeam {
        return HomeTeam(
            response._id,
            response.name,
            response.shortName
        )
    }

    private fun mapLocation(response: LocationData): Location {
        return Location(response.original)
    }

    private fun mapMatchDay(response: MatchDayData): MatchDay {
        return MatchDay(
            response.end,
            mapName(response.name),
            mapPhase(response.phase),
            response.start
        )
    }

    private fun mapPhase(response: PhaseData): Phase {
        return Phase(response.original)
    }

    override fun map(response: EventListData): EventList {
        return EventList(
            items = mapItemList(response.items),
            pagination = Pagination(
                first = response.pagination.first,
                items = response.pagination.items,
                last = response.pagination.last,
                next = response.pagination.next,
                offset = response.pagination.offset,
                page = response.pagination.page,
                pages = response.pagination.pages,
                totalItems = response.pagination.totalItems
            ),
            sections = mapSelectionList(response.sections)
        )
    }

}