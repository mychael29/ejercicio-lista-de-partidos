package com.meza.data.mapper

import com.meza.data.network.response.EventListData
import com.meza.domain.entity.EventList

interface EventsMapper {
    fun map(response: EventListData): EventList
}