package com.meza.data.mapper

import com.meza.data.network.response.TeamData
import com.meza.data.network.response.TeamListData
import com.meza.domain.entity.Team
import com.meza.domain.entity.TeamList

class FilterMapperImpl: FilterMapper {
    override fun mapListTeam(responseList: TeamListData): TeamList {
        return TeamList(mapListTeamList(responseList.items))
    }
    private fun mapListTeamList(responseList: List<TeamData>): List<Team> {
        return responseList.map { (mapTeam(it)) }
    }

    private fun mapTeam(response: TeamData): Team {
        return Team(
            _id = response._id,
            id = response.id,
            name = response.name,
            shortName = response.shortName
        )
    }
}