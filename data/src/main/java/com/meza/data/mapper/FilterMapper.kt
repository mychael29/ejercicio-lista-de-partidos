package com.meza.data.mapper

import com.meza.data.network.response.TeamListData
import com.meza.domain.entity.TeamList


interface FilterMapper {
    fun mapListTeam(responseList: TeamListData): TeamList
}