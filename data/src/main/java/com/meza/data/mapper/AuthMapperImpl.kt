package com.meza.data.mapper

import com.meza.data.network.request.*
import com.meza.data.network.response.AuthData
import com.meza.domain.entity.Authenticator
import com.meza.domain.entity.Authenticate
import com.meza.domain.entity.Profile
import com.meza.domain.entity.Rbac
import com.meza.domain.entity.User

class AuthMapperImpl: AuthMapper {

    override fun mapResponse(response: AuthData): Authenticator {
        return Authenticator(
            accessToken = response.accessToken,
            expiresIn = response.expiresIn,
            tokenType = response.tokenType,
            user = User(
                response.user._id,
                Profile(response.user.profile.language),
                Rbac(
                    response.user.rbac.role,
                    response.user.rbac.template
                )
            )
        )
    }

    override fun mapRequest(entity: Authenticate): AuthRequest {
        return AuthRequest(
            device = DeviceRequest(
            deviceId = entity.device.deviceId,
            heigth = entity.device.heigth,
            model = entity.device.model,
            name = entity.device.name,
            platform = entity.device.platform,
            version = entity.device.version,
            width = entity.device.width),
            app = AppRequest(entity.app.version),
            user =  UserRequest(_id = "", profile = ProfileRequest(entity.user.profile.language), rbac = Rbac("", ""))
        )
    }
}