package com.meza.data.network.request

import com.meza.domain.entity.Rbac

data class AuthRequest(
    val app: AppRequest,
    val device: DeviceRequest,
    val user: UserRequest
)

data class AppRequest(
    val version: String
)

data class DeviceRequest(
    val deviceId: String,
    val heigth: String,
    val model: String,
    val name: String,
    val platform: String,
    val version: String,
    val width: String
)

data class UserRequest(
    val _id: String,
    val profile: ProfileRequest,
    val rbac: Rbac
)

data class ProfileRequest(
    val language: String
)

