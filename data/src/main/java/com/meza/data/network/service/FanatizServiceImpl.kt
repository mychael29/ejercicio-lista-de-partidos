package com.meza.data.network.service

import com.meza.domain.entity.Result
import com.meza.data.network.apis.FanatizApi
import com.meza.data.network.request.AuthRequest
import com.meza.data.network.response.*
import com.meza.domain.Constants.STATUS
import com.meza.domain.Constants.TEAMS
import com.meza.domain.Failure
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONObject
import retrofit2.Response
import java.net.SocketTimeoutException
import javax.net.ssl.SSLException
import javax.net.ssl.SSLHandshakeException

class FanatizServiceImpl(private val fanatizApi: FanatizApi): FanatizService {

    override suspend fun authenticate(authenticate: AuthRequest): Result<AuthResponse, Failure> {
        return apiCall2(call = { fanatizApi.authenticateAsync(authenticate) })
    }

    override suspend fun getEventList(accessToken: String, filter: List<String>): Result<EventResponse, Failure> {
        return apiCall2(call = { fanatizApi.getEventListAsync("Bearer $accessToken", filter[STATUS], filter[TEAMS]) })
    }

    override suspend fun getTeamList(accessToken: String): Result<TeamResponse, Failure> {
        return apiCall2(call = { fanatizApi.getTeamListAsync("Bearer $accessToken") })
    }

    private suspend inline fun <T : Any> apiCall2(crossinline call: suspend () -> Response<T>): Result<T, Failure> {
        val response: Response<T>

        try {
            response = call.invoke()
        } catch (throwable: Throwable) {
            return Result.Error(parseException(throwable))
        }

        return if (response.isSuccessful && response.body() != null) {
            Result.Success(response.body()!!)
        } else {
            Result.Error(
                getErrorMessageFromServer(
                    response.errorBody()?.string()
                )
            )
        }
    }

    private suspend fun getErrorMessageFromServer(errorBody: String?) : Failure {
        return if (errorBody != null) {
            return withContext(Dispatchers.IO) {
                try {
                    val data = fromJson<ErrorResponse>(JSONObject(errorBody).toString())
                    when (data.error.code) {
                        4014 -> {
                            return@withContext Failure.UnauthorizedOrForbidden(data.error.userMessage.es)
                        }
                        4017 -> {
                            return@withContext Failure.UnauthorizedOrForbidden(data.error.userMessage.es)
                        }
                        5001 -> {
                            return@withContext Failure.ServerError(data.error.userMessage.es)
                        }
                        else -> {
                            return@withContext Failure.ServiceUncaughtFailure(data.error.userMessage.es)
                        }
                    }
                } catch (e: Exception) {
                    return@withContext Failure.ServerBodyError(e.message.toString())
                }
            }
        } else {
            Failure.None
        }
    }

    private fun parseException(throwable: Throwable) : Failure {
        return when(throwable) {
            is SocketTimeoutException -> Failure.TimeOut
            is SSLException -> Failure.NetworkConnectionLostSuddenly
            is SSLHandshakeException -> Failure.SSLError
            else -> Failure.ServiceUncaughtFailure(throwable.message?:"Service response doesn't match with response object.")
        }
    }
}