package com.meza.data.network.service

import com.meza.domain.entity.Result
import com.meza.data.network.request.AuthRequest
import com.meza.data.network.response.AuthResponse
import com.meza.data.network.response.EventResponse
import com.meza.data.network.response.TeamResponse
import com.meza.domain.Failure

interface FanatizService {

    suspend fun authenticate(authenticate: AuthRequest): Result<AuthResponse, Failure>

    suspend fun getEventList(accessToken: String, filter: List<String>): Result<EventResponse, Failure>

    suspend fun getTeamList(accessToken: String): Result<TeamResponse, Failure>

}