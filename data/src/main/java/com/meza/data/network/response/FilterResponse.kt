package com.meza.data.network.response

import com.meza.domain.entity.Pagination

data class TeamListData(
    val items: List<TeamData>,
    val pagination: Pagination
)

data class TeamData(
    val _createDate: String,
    val _deleted: Boolean,
    val _externalId: String,
    val _externalProvider: String,
    val _id: String,
    val _model: String,
    val _seed: Boolean,
    val _updateDate: String,
    val id: String,
    val name: String,
    val shortName: String
)