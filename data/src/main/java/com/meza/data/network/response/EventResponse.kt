package com.meza.data.network.response


data class EventListData(
    val items: List<EventData>,
    val pagination: PaginationData,
    val sections: List<SectionData>
)

data class EventData(
    val __belong: BelongData,
    val __children: ChildrenData,
    val _createDate: String,
    val _externalId: String,
    val _externalProvider: String,
    val _id: String,
    val _model: String,
    val awayPenalties: Any,
    val awayScore: Int,
    val awayTeam: AwayTeamData,
    val cellType: Int,
    val endDate: String,
    val evenPenalties: Any,
    val eventStatus: EventStatusData,
    val homeScore: Int,
    val homeTeam: HomeTeamData,
    val location: LocationData,
    val matchDay: MatchDayData,
    val matchTime: Int,
    val startDate: String,
    val statusCategory: String,
    val statusDate: String,
    val type: String
)

data class PaginationData(
    val first: Int,
    val items: Int,
    val last: Int,
    val next: Int,
    val offset: Int,
    val page: Int,
    val pages: Int,
    val totalItems: Int
)

data class SectionData(
    val _id: String,
    val index: Int,
    val title: TitleData
)

data class BelongData(
    val AccessGroup: List<String>,
    val Client: String,
    val Tournament: List<String>
)

data class ChildrenData(
    val Formation: List<String>,
    val TimeLine: List<String>
)

data class EventStatusData(
    val _id: String,
    val category: String,
    val name: NameData
)

data class HomeTeamData(
    val _id: String,
    val name: String,
    val shortName: String
)

data class AwayTeamData(
    val _id: String,
    val name: String,
    val shortName: String
)

data class LocationData(
    val original: String
)

data class MatchDayData(
    val end: String,
    val name: NameData,
    val phase: PhaseData,
    val start: String
)

data class NameData(
    val es: String? = null,
    val original: String
)

data class PhaseData(
    val original: String
)

data class TitleData(
    val original: String
)