package com.meza.data.network.response

data class AuthData(
    val accessToken: String,
    val expiresIn: String,
    val tokenType: String,
    val user: UserData
)

data class UserData(
    val _id: String,
    val profile: ProfileData,
    val rbac: RbacData
)

data class ProfileData(
    val language: String
)

data class RbacData(
    val role: String,
    val template: String
)