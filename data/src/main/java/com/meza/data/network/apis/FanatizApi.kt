package com.meza.data.network.apis

import com.meza.data.network.request.AuthRequest
import com.meza.data.network.response.*
import retrofit2.Response
import retrofit2.http.*

interface FanatizApi {

    @POST("auth/users/login/anonymous")
    suspend fun authenticateAsync(
        @Body body: AuthRequest
    ): Response<AuthResponse>

    @GET("sport/events")
    suspend fun getEventListAsync(
        @Header("Authorization") accessToken: String,
        @Query("filterBy-statusCategory") filterBy: String,
        @Query("filterTeams") filterTeams: String
    ): Response<EventResponse>

    @GET("sport/teams")
    suspend fun getTeamListAsync(
        @Header("Authorization") accessToken: String
    ): Response<TeamResponse>

}