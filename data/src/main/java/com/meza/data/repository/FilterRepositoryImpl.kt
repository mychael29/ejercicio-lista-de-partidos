package com.meza.data.repository

import com.meza.data.mapper.FilterMapper
import com.meza.domain.entity.Result
import com.meza.data.network.service.FanatizService
import com.meza.domain.Failure
import com.meza.domain.entity.Status
import com.meza.domain.entity.StatusList
import com.meza.domain.entity.TeamList
import com.meza.domain.repository.FilterRepository
import javax.inject.Inject

class FilterRepositoryImpl @Inject constructor(
    private val service: FanatizService,
    private val mapper: FilterMapper
): FilterRepository {

    override suspend fun getTeamList(accessToken: String): Result<TeamList, Failure> {
        return when (val result = service.getTeamList(accessToken)) {
            is Result.Success -> Result.Success(mapper.mapListTeam(result.result.data))
            is Result.Error -> Result.Error(result.error)
        }
    }

    override suspend fun getStatusList(accessToken: String): Result<StatusList, Failure> {
        return Result.Success(StatusList(
            mutableListOf(
                Status("1", "live"),
                Status("2", "finished"),
                Status("3", "future")
            )
        ))
    }

}