package com.meza.data.repository

import android.util.Log
import com.meza.data.mapper.EventsMapper
import com.meza.domain.entity.Result
import com.meza.data.network.service.FanatizService
import com.meza.domain.Constants.STATUS
import com.meza.domain.Constants.TEAMS
import com.meza.domain.Failure
import com.meza.domain.entity.*
import com.meza.domain.repository.EventRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class EventRepositoryImpl @Inject constructor(
    private val service: FanatizService,
    private val mapper: EventsMapper
): EventRepository {

    override suspend fun getEvents(accessToken: String, filter: List<String>): Result<EventList, Failure> {
        Log.d("TAG", "FILTER > " + filter[STATUS] + " - " + filter[TEAMS])
        return when (val result  = service.getEventList(accessToken, filter)) {
            is Result.Success -> Result.Success(mapper.map(result.result.data))
            is Result.Error -> Result.Error(result.error)
        }
    }

}